/**
 * File     BMI.cpp
 * @author  ThanhTungYL
 */

#include "iostream"
#include "cmath"

using namespace std;

int main() {
    double cc, cn;
    cin >> cc;
    cin >> cn;
    cout << cn / pow(cc, 2) << endl;
    return 0;
}
