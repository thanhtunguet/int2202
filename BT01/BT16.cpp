/**
 * File     BT16.cpp
 * @author  ThanhTungYL
 */

 #include "iostream"

 using namespace std;

 int main() {
    int x, y, z;
    cin >> x >> y >> z;
    if (x == y && y == z)
        cout << "TRUE";
    else
        cout << "FALSE";
    return 0;
 }
