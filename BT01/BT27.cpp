/**
 * File     BT27.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

int main() {
    int N;
    while (true) {
        cin >> N;
        cout << ((N >= 0 && N % 5 == 0) ? N / 5 : -1) << endl;
    }
}
