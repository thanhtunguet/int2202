/**
 * File     BT27_after-edited.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

int main() {
    int N;
    while (true) {
        cin >> N;
        if (N != -1)
            cout << ((N >= 0 && N % 5 == 0) ? N / 5 : -1) << endl;
        else
            break;
    }
}
