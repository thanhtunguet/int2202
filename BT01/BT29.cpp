/**
 * File     BT29.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

int inSao(int N) {
    for (int i = 0; i < N; i++) {
        cout << "*";
    }
}

int main() {
    int N;
    cin >> N;
    cout << "*";
    for (int i = 2; i <= N; i++) {
        cout << endl;
        inSao(i);
    }

    return 0;
}
