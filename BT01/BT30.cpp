/**
 * File     BT30.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

int inSao(int N) {
    for (int i = 0; i < N; i++) {
        cout << "*";
    }
}

int main() {
    int N;
    cin >> N;
    for (int i = N; i > 1; i--) {
        inSao(i);
        cout << endl;
    }
    cout << "*";
    return 0;
}
