/**
 * File     BT26.cpp - BasicStatics.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

int main() {
    int N, _max, _min, _input;
    double _mean;
    cin >> N;
    cin >> _input;
    _max = _min = _input;
    _mean = (double) _input;
    for (int i = 2; i <= N; i++) {
        cin >> _input;
        if (_input < _min) {
            _min = _input;
        } else if (_input > _max) {
            _max = _input;
        }
        _mean += ((double) _input);
    }
    _mean = _mean / (double) N;
    cout << "Mean: " << _mean << endl;
    cout << "Min : " << _min  << endl;
    cout << "Max : " << _max  << endl;
}
