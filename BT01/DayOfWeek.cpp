/**
 * File     DayOfWeek.cpp
 * @author  ThanhTungYL
 */

#include "iostream"
#include "ctime"

 using namespace std;

int main() {
    int da, mo, ye;
    cin >> da >> mo >> ye;

    struct tm *My_Time;

    time_t rtm;
    time(&rtm);

    My_Time = localtime(&rtm);

    My_Time->tm_year = ye - 1900;
    My_Time->tm_mon  = mo;
    My_Time->tm_mday = da;

    mktime(My_Time);

    switch (My_Time->tm_wday) {
    case 0:
        cout << "Sunday";
        break;
    case 1:
        cout << "Monday";
        break;
    case 2:
        cout << "Tuesday";
        break;
    case 3:
        cout << "Wednesday";
        break;
    case 4:
        cout << "Thursday";
        break;
    case 5:
        cout << "Friday";
        break;
    case 6:
        cout << "Saturday";
        break;
    }
}
