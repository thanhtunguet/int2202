/**
 * File     Divisibility.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

int main() {
    int A, B;
    cin >> A >> B;
    if (A % 7 == 0 && B % 7 == 0) {
        cout << "TRUE";
    } else {
        cout << "FALSE";
    }
    return 0;
}
