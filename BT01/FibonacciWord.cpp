/**
 * File     FibonacciWord.cpp
 * @author  ThanhTungYL
 */

#include "iostream"

using namespace std;

string Fbnc(int N) {
    if (N == 0) {
        return "a";
    } else if (N == 1) {
        return "b";
    } else if (N > 1) {
        return Fbnc(N-1) + Fbnc(N-2);
    }
}

int main() {
    for (int i = 0; i <= 10; i++) {
        cout << Fbnc(i) << endl;
    }
    return 0;
}
