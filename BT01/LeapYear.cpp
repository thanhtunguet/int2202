/**
 * File     LeapYear.cpp
 * @author  ThanhTungYL
 */

 #include "iostream"

 using namespace std;

 int main() {
    int ye;
    cin >> ye;
    if (ye % 400 == 0 || (ye % 4 == 0 && ye % 100 != 0))
        cout << "TRUE";
    else
        cout << "FALSE";
    return 0;
 }
