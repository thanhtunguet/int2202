/**
 * File     BT23.cpp - MaxWithoutIf.cpp
 * @author  ThanhTungYL
 */

#include "iostream"
#include "cmath"

using namespace std;

int main() {
    int A, B;
    cin >> A >> B;
    cout << (A + B + abs(A-B)) / 2 << endl;
    return 0;
}
