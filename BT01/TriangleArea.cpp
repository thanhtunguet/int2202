/**
 * File     TriangleArea.cpp
 * @author  ThanhTungYL
 */

#include "iostream"
#include "cmath"

using namespace std;

int main() {
    double a, b, c;
    cin >> a >> b >> c;
    #define S (a+b+c)/2
    cout << sqrt(S * (S-a) * (S-b) * (S-c)) << endl;
    return 0;
}
