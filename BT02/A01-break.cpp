#include "iostream"

using namespace std;

int main() {
	int N = 2;
	while (true) {
		if (N > 100) {
			break;
		} else {
			cout << N;
			if (N < 100)
				cout << " ";
			N += 2;
		}
	}
	return 0;
}
