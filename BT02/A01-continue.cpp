#include "iostream"

using namespace std;

int main() {
	int N = 0;
	while (true) {
		N++;
		if (N > 100) {
			break;
		} else {
			if (N % 2 == 0) {
				cout << N;
				if (N < 100) {
					cout << " ";
				}
			} else {
				continue;
			}
		}
	}
}
