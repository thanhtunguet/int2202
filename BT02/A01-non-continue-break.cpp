#include "iostream"

using namespace std;

void printUntil(int N = 2) {
	if (N > 0 && N <= 100 && N % 2 == 0) {
		cout << N;
		if (N < 100) {
			cout << " ";
			printUntil(N + 2);
		}
	}
}

int main() {
	printUntil();
	return 0;
}
