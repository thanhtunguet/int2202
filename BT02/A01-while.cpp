#include "iostream"

using namespace std;

/**
 * Theo ý kiến của mình thì đây
 * là cách làm tốt nhất, bởi vì
 * đây là bài toán rất đơn giản
 * điều kiện chỉ là N chẵn dương
 * và nhỏ hơn 100. Chỉ cần tăng
 * dần N 2 đơn vị, kiểm tra điều
 * kiện nhỏ hơn 100 là được
 */

int main() {
	// Khong xac dinh so lan lap
	int N = 2;
	while (N <= 100) {
		cout << N;
		if (N < 100)
			cout << " ";
		N += 2;
	}
	return 0;
}
