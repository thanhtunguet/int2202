#include "iostream"

using namespace std;

int printChar(char c, int nums) {
    for (int i = 0; i < nums; i++)
        cout << c;
}

int main() {
    int N;
    cin >> N;
    for (int i = 0; i < N; i++) {
        printChar(' ', i);
        printChar('*', N - i);
        cout << endl;
    }
    return 0;
}
