#include "iostream"

using namespace std;

int printChar(char c, int nums) {
    for (int i = 0; i < nums; i++)
        cout << c;
}

int main() {
    int N;
    cin >> N;
    N--;
    for (int i = 0; i <= N; i++) {
        printChar(' ', N - i);
        printChar('*', 2 * i + 1);
        cout << endl;
    }
    return 0;
}
