#include "iostream"

using namespace std;
int main() {

    int N;
    cin >> N;
    int i, j;
    for (i = 0; i < N; i++) {
        cout << i % N + 1;
        for (j = 1; j < N; j++) {
            cout << " " << (i + j) % N + 1;
        }
        cout << endl;
    }
}
