#include "iostream"
using namespace std;

const int N = 4;

char outStr[N];
char outStr_b_1[N+2] = "abcd";
/**
 * char outStr_b_2[N-1] = "abcd";
 * Co loi
 */

/**
 * char outStr_b_3[N] = "abcd";
 * Co loi
 */
 char outStr_c[] = "abcd";
int main() {
	char inStr[N];
	char inStr_b_1[N+2] = "abcd";
//	char inStr_b_2[N-1] = "abcd";
//	char inStr_b_3[N] = "abcd";
	char inStr_c[] = "abcd";
	/** Phan A */
	for (int i = 0; i < N; i++) {
		cout << inStr[i];
	}
	cout << endl << inStr << endl;

	for (int i = 0; i < N; i++) {
		cout << outStr[i];
	}
	cout << endl << outStr << endl;
	/** Phan B */
	for (int i = 0; i < N + 2; i++) {
		cout << outStr_b_1[i];
	}
	cout << endl << outStr_b_1 << endl;
	for (int i = 0; i < N + 2; i++) {
		cout << inStr_b_1[i];
	}
	cout << endl << inStr_b_1 << endl;
	/** Phan C */
	for (int i = 0; i < N; i++) {
		cout << outStr_c[i];
	}
	cout << endl << outStr_c << endl;
	for (int i = 0; i < N; i++) {
		cout << inStr_c[i];
	}
	cout << endl << inStr_c << endl;
	cout << sizeof(outStr_c) << " " << sizeof(inStr_c);
}
