#include "iostream"
using namespace std;

/**
 * Các khai báo bị lỗi
 * được chuyển thành comment
 */

int cach1() {
	char daytab[2][12] = {
		{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
		{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}

int cach1_b() {
	char daytab[2][12] = {
		{31, 28, 31, 30, 31, 30, 31, 31},
		{31, 29, 31}
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}

int cach1_bo_hang() {
	char daytab[][12] = {
		{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
		{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}

/*int cach1_bo_cot() {
	char daytab[2][] = {
		{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
		{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}
*/

/* int cach1_bo_ca_hai() {
	char daytab[][] = {
		{31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
		{31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}
 */

int cach2() {
	char daytab[2][12] = {
		31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
		31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}

int cach2_b() {
	char daytab[2][12] = {
		31, 28, 31, 30, 31, 30, 31,
		31, 29, 31, 30, 31, 30
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}

int cach2_bo_hang() {
	char daytab[][12] = {
		31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
		31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}

/* int cach2_bo_cot() {
	char daytab[2][] = {
		31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
		31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}
 */

/* int cach2_bo_ca_hai() {
	char daytab[][] = {
		31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
		31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
	};
	for (int i = 0; i < 2; i++) {
		if (i > 0)
			cout << endl;
		for (int j = 0; j < 12; j++) {
			if (j > 0)
				cout << " ";
			cout << daytab[i][j];
		}
	}
}
 */

int main() {
	/** Gọi các hàm theo từng cách */
	// Ví dụ:
	cach1();
}
