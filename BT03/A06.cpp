#include "iostream"
#include "ctime"
#include "cstdlib"
using namespace std;

int myRand() {
	srand(rand() + time(NULL));
	return rand() % 100 + 1;
}

void doi_cho(int *a, int *b) {
	int c = *a;
	*a = *b;
	*b = c;
}

const int N = 30;
int main() {
	int mang[N];
	for (int i = 0; i < 30; i++) {
		if (i > 0)
			cout << " ";
		mang[i] = myRand();
		cout << mang[i];
	}
	for (int i = 0; i < N; i++) {
		for (int j = 0; j <= i; j++) {
			if (mang[i] < mang[j]) {
				doi_cho(&mang[i], &mang[j]);
			}
		}
	}
	cout << endl;
	for (int i = 0; i < 30; i++) {
		if (i > 0)
			cout << " ";
		cout << mang[i];
	}
}
