#include "iostream"
using namespace std;

int main() {
    int N;
    cin >> N;
    int series[N];
    for (int i = 0; i < N; i++) {
		series[i] = 0;
		do {
			cin >> series[i];
		} while (series[i] < 1 || series[i] > N);
    }
	char seen[N];
    for (int i = 0; i < N; i++) {
		seen[i] = 0;
    }
    for (int i = 0; i < N; i++) {
		seen[series[i]-1]++;
    }
    bool space = false;
    for (int i = 0; i < N; i++) {
		if (seen[i] > 1) {
			if (! space) {
				space = true;
			} else {
				cout << " ";
			}
			cout << i+1;
		}
    }
}
