#include "iostream"
#include "cstdio"
#include "cstring"
#include "iostream"
using namespace std;
const int N = 100;

bool symmetric_string(char *c) {
	int len = strlen(c);
	bool symmetric = true;
	int i = 0, j = len - 1;
	while (j - i > 1) {
        if (c[i] != c[j]) {
			symmetric = false;
			break;
        }
        i++;
        j--;
	}
	return symmetric;
}

int main() {
	char c, str[N];
	int i = 0;
	while (c = fgetc(stdin)) {
		if (c == '\n') {
			break;
		} else if (i < N-1) {
			str[i] = c;
			i++;
		}
	}
	str[i] = '\0';
	if (symmetric_string(str)) {
		cout << "Yes";
	} else {
		cout << "No";
	}
}
