#include "iostream"
using namespace std;

bool so_doi_guong(int N) {
	// Kiem tra so doi guong
	int newN = 0, chuyen_doi = N;
	// Dao nguoc so N
	while (chuyen_doi > 0) {
		newN = newN * 10 + chuyen_doi % 10;
		chuyen_doi /= 10;
	}
	if (newN == N)
		return true;
	else
		return false;
}

int main() {
	int A, B;
	cin >> A >> B;
	int nums = 0;
	for (int i = A; i <= B; i++) {
		if (so_doi_guong(i)) {
			nums++;
		}
	}
	cout << nums;
}
