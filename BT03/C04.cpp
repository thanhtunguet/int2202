#include "iostream"
using namespace std;

int main() {
	int m, n;
	cin >> m >> n;
	char input[m][n];
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < n; j++) {
			cin >> input[i][j];
		}
	}
	int mine;
	for (int i = 0; i < m; i++) {
		if (i > 0) {
			cout << endl;
		}
		for (int j = 0; j < n; j++) {
			if (j > 0) {
				cout << " ";
			}
			if (input[i][j] == '*') {
				cout << '*';
			} else {
				mine = 0;
                for (int a = i-1; a <= i+1; a++) {
					for (int b = j-1; b <= j+1; b++) {
						if (a >= 0 && a < m && b >= 0 && b < n && input[a][b] == '*') {
							mine++;
						}
					}
                }
                cout << mine;
			}
		}
	}
}
