#include "iostream"
using namespace std;

int main() {
	int x, y;
	cin >> x >> y;
	int tich = x*y;
	int i = 1;
	int sx = 0,		// Start X
		sy = 0,		// Start y
		ex = x-1,	// End x
		ey = y-1;	// End y
	int a = sx, b = sy;
	int screen[x][y];
	while (i <= tich) {
		for (b = sy; b <= ey; b++) {
			screen[a][b] = i;
			i++;
		}
		sx++;
		for (a = sx; a <= ex; a++) {
			screen[a][b] = i;
			i++;
		}
		ey--;
		for (b = ey; b >= sy; b--) {
			screen[a][b] = i;
			i++;
		}
		ex--;
		for (a = ex; a >= sx; a--) {
			screen[a][b] = i;
			i++;
		}
		sy++;
	}
	for (a = 0; a < x; a++) {
		if (a > 0) cout << endl;
		for (b = 0; b < y; b++) {
			if (b > 0)
			   cout << " ";
   			cout << screen[a][b];
		}
	}
	return 0;
}

