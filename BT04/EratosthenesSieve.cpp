#include "iostream"
#include "cstdio"
using namespace std;

int main() {
	int N;
	cin >> N;
	int ds[N-1];
	for (int i = 2; i <= N; i++) {
		ds[i-2] = i;
	}
	for (int i = 2; i <= N; i++) {
		if (ds[i-2] > 0) {
			for (int del = 2*i; del <= N; del = del+i) {
				ds[del-2] = -1;
			}
		}
	}
	bool newspace = false;
	for (int i = 2; i <= N; i++) {
		if (ds[i-2] > 0) {
			if (newspace) {
				cout << " ";
			} else {
				newspace = true;
			}
			cout << i;
		}
	}
	return 0;
}

