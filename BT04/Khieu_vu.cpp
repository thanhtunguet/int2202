#include "iostream"
using namespace std;
const int MAX_NUM = 10000;

void bubble_sort(double *A, int len) {
	bool da_sx = false;
	for (int i = 0; i < len; i++) {
		if (! da_sx) {
			for (int j = i; j < len; j++) {
				if (A[i] > A[j]) {
					da_sx = true;
					int c	= A[i];
					A[i]	= A[j];
					A[j]	= c;
				}
			}
		} else {
			break;
		}
	}
}

int main() {
	int N = 0;
	 do {
	 	cin >> N;
	 } while (N <= 0 || N > MAX_NUM);
	double cc_nam[MAX_NUM], cc_nu[MAX_NUM];
	for (int i = 0; i < N; i++) {
		cin >> cc_nam[i] >> cc_nu[i];
	}
	bubble_sort(cc_nam, N);
	bubble_sort(cc_nu, N);
	bool SapXep = true;
	for (int i = 0; i < N; i++) {
		if (cc_nu[i] >= cc_nam[i]) {
			SapXep = false;
			break;
		}
	}
	cout << (SapXep ? "YES" : "NO");
	return 0;
}

