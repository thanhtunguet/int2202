#include "iostream"
#include "cstring"
using namespace std;

const int MAX_CHARS_LENGTH = 25;

void toLowerCase(char *s) {
	int len = strlen(s);
	for (int i = 0; i < len; i++) {
		if (s[i] >= 'A' && s[i] <= 'Z') {
			s[i] += 32;
		}
	}
}

int main() {
	char input[MAX_CHARS_LENGTH];
	cin >> input;
	toLowerCase(input);
	int len = strlen(input);
	bool newline = false;
	for (int i = 0; i < len; i++) {
		for (int j = 0; j < len; j++) {
			if (newline) {
				cout << endl;
			} else {
				newline = true;
			}
			cout << input[i] << input[j];
			for (int k = 0; k < len; k++) {
				if (newline) {
					cout << endl;
				} else {
					newline = true;
				}
				cout << input[i] << input[j] << input[k];
			}
		}
	}
	return 0;
}

