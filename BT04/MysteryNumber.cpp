#include "iostream"
using namespace std;
const int MAX_ARRAY_LENGTH = 100;

void bubble_sort(int *A, int len) {
	bool da_sx = false;
	for (int i = 0; i < len; i++) {
		if (! da_sx) {
			for (int j = i; j < len; j++) {
				if (A[i] > A[j]) {
					da_sx = true;
					int c	= A[i];
					A[i]	= A[j];
					A[j]	= c;
				}
			}
		} else {
			break;
		}
	}
}

int main() {
	int N = 0;
	do {
		cin >> N;
	} while (N <= 0 || N > MAX_ARRAY_LENGTH);
	int A[MAX_ARRAY_LENGTH], B[MAX_ARRAY_LENGTH+1];
	for (int i = 0; i < N; i++) {
		cin >> A[i];
	}
	for (int i = 0; i <= N; i++) {
		cin >> B[i];
	}
	bubble_sort(A, N);
	bubble_sort(B, N + 1);
	int diff = -1;
	for (int i = 0; i < N; i++) {
		if (A[i] != B[i]) {
			diff = i;
			break;
		}
	}
	if (diff == -1)
		diff = N;
	cout << B[diff];
	return 0;
}

