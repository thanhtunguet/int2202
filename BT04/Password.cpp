#include "iostream"
#include "cstring"
using namespace std;
const int MAX_PASSWORD_LENGTH = 100;
const int MAX_NUMS_PASSWORD = 100;
typedef char password[MAX_PASSWORD_LENGTH];

bool is_strrev(const char *s1, const char *s2) {
	int len = strlen(s1);
	if (strlen(s2) != len) {
		return false;
	} else {
		bool is_rev = true;
		for (int i = 0; i < len; i++) {
			if (s1[i] != s2[len-1-i]) {
				is_rev = false;
				break;
			}
		}
		return is_rev;
	}
}

int main() {
	int N = 0;
	do {
		cin >> N;
	} while (N <= 0 || N > MAX_NUMS_PASSWORD);
	password pswd[MAX_NUMS_PASSWORD];
	for (int i = 0; i < N; i++) {
		cin >> pswd[i];
	}
	for (int i = 0; i < N; i++) {
		for (int j = i+1; j < N; j++) {
			if (is_strrev(pswd[i], pswd[j])) {
				cout << strlen(pswd[i]) << " " << pswd[i][strlen(pswd[i])/2];
				break;
			}
		}
	}
	return 0;
}
