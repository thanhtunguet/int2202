#include "iostream"
using namespace std;

const int MAX_ARRAY_LENGTH = 100000;
const int MAX_TESTS = 10;
int main() {
	int numsOfTest = 0;
	long N = 0;
	do {
		cin >> numsOfTest;
	} while (numsOfTest <= 0 || numsOfTest > MAX_TESTS);
	for (int forTest = 0; forTest < numsOfTest; forTest++) {
		if (forTest > 0)
			cout << "\n"; // Them newline de phan tach cac test
		do {
			cin >> N;
		} while (N <= 0 || N > MAX_ARRAY_LENGTH);
		int A[MAX_ARRAY_LENGTH];
		int sumA = 0, sumLeft = 0, sumRight = 0;
		bool sumEqual = false;
		for (int i = 0; i < N; i++) {
			cin >> A[i];
			sumA += A[i];
		}
		for (int i = 0; i < N; i++) {
			if (i > 0)
				sumLeft += A[i-1];
				sumRight = sumA - A[i] - sumLeft;
				if (sumRight == sumLeft) {
					sumEqual = true;
					break;
				}
		}
		cout << (sumEqual ? "YES" : "NO");
	}
	return 0;
}

