#include "iostream"
using namespace std;
const int MAX_NUM = 10000;

int main() {
	int N = 0;
	 do {
	 	cin >> N;
	 } while (N <= 0 || N > MAX_NUM);
	int series[MAX_NUM];
	for (int i = 0; i < N; i++) {
		cin >> series[i];
	}
	bool newline = false;
	for (int i = 0; i < N; i++) {
		for (int j = i + 1; j < N; j++) {
			for (int k = j + 1; k < N; k++) {
				if (series[i] + series[j] + series[k] == 0) {
					if (! newline) {
						newline = true;
					} else {
						cout << endl;
					}
					cout << series[i] << " " << series[j] << " " << series[k];
				}
			}
		}
	}
	return 0;
}

