#include "iostream"
using namespace std;

void function3(int A, int& B) {
	cout << (long long) &A << " " << (long long) &B;
}

int main() {
	int A;
	int&B = A;
	function3(A, B);
	cout << endl;
	cout << (long long) &A << " " << (long long) &B;
	/**
	 * Output cho thay su khac nhau giua dia chi cua
	 * tham tri va doi so, va su giong nhau cua tham
	 * bien va doi so.
	 * Nghia la: khi truyen tham tri vao ham, thi gia
	 * tri cua tham tri do duoc sao chep sang mot vung
	 * bo nho khac nen co dia chi khac, con khi truyen
	 * tham bien vao thi dia chi cua bien do duoc truyen
	 * vao ham va ham se su dung vung bo nho do de thuc
	 * hien cac hanh dong
	 */
	return 0;
}

