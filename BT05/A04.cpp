#include "iostream"
using namespace std;
const int N = 5;

void function4(int A[N], string str) {
	cout << (long long) A << " " << (long long) &str;
}

int main() {
	int A[N];
	string str;
	function4(A, str);
	cout << endl;
	cout << (long long) A << " " << (long long) &str;	
	/**
	 * Array:		pass-by-reference
	 * C++ String:	pass-by-value
	 */
	return 0;
}

