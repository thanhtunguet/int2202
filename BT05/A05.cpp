#include "iostream"
using namespace std;

int main() {
	int A;
	int& B = A;
	cout << (long long) &A << " " << (long long) &B;
/**
 * Bien tham chieu B va bien A duoc B chieu toi la mot
 */
// 	int& C;
// 	printf("\n%i", &C);

/**
 * Khong the khai bao tham chieu ma khong chieu ngay
 * toi mot bien thuong.
 * Trinh dich bao loi:
 * [Error] 'C' declared as reference but not initialized
 */
 	int C = 5;
 	B = C;
 	A = 3;
 	cout << endl << B;
/**
 * Khong the thay doi dich tham chieu
 * Ban dau B chieu toi A
 * Sau gan B = C hay co nghia la B chieu toi C
 * Gan A = 3
 * Lay gia tri cua B thi cho ket qua bang voi A
 * A = B = 3; C = 5
 * Nghia la khong co phep thay doi dich chieu
 * Sau khi chieu B toi A thi moi phep tinh voi B
 * deu tro thanh phep tinh voi vung bo nho luu A
 */
	return 0;
}

