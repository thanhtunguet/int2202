#include "iostream"
using namespace std;
const int N = 5;
int main() {
	int n1;
	char c1;
	int A[N];
	char B[N];
	char c2;
	cout << (long long) &n1 << " " << (long long) &c1 << endl;
	cout << (long long) &A[0] << " " << (long long) &A[1] << " " << (long long) &A[2] << endl;
	cout << (long long) &B[0] << " " << (long long) &B[1] << " " << (long long) &B[2] << endl;
	cout << ((long long) &c2);
	/**
	 * Dia chi cac bien kieu int va kieu char
	 * khac nhau la do kich thuoc cua kieu du
	 * lieu khac nhau. 1 gia tri int duco luu
	 * boi 4bytes con 1 gia tri char duoc luu
	 * boi 1byte
	 * Rieng cac phan tu trong mang thi duoc luu
	 * dia chi tang dan
	 * Cac bien khai bao truoc co dia chi bo nho
	 * lon hon, chung to rang dia chi o nho duoc
	 * sap xep giam dan
	 */
	/**
	 * Vi du 1 output:
	 * 7339596 7339595			//n1 duoc khai bao truoc c1
	 * 7339568 7339572 7339576	// Cac phan tu cua mang int A
	 * 7339552 7339553 7339554	// Cac phan tu cua mang char B
	 * 7339551					// c2 duoc khai bao sau cung
	 */
	return 0;
}
