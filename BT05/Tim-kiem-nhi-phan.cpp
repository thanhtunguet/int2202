#include "iostream"
#include "ctime"
using namespace std;

const int _BEG = 0;
const int _END = 100;

int search_loop(int low, int high, int key, int *A) {
	int mid;
	do {
		if (A[low] > key || A[high] < key) {
			return -1;
		} else {
			mid = (low+high)/2;
			if (A[mid] == key) {
				return mid;
			} else if (A[mid] > key) {
				high = mid - 1;
			} else {
				low = mid + 1;
			}
		}
	} while (low < high);
}

int search_recursive(int low, int high, int key, int *A) {
	int mid;
	if (A[low] > key || A[high] < key) {
		return -1;
	} else {
		mid = (low+high) / 2;
		if (A[mid] == key) {
			return mid;
		} else if (A[mid] > key) {
			return search_recursive(low, mid-1, key, A);
		} else {
			return search_recursive(mid+1, high, key, A);
		}
	}
}

int main() {
	int A[_END];
	
	// Hardcode cac gia tri trong mang
	
	for (int i = _BEG; i < _END; i++) {
		A[i] = i * 3;
	}
	clock_t t = clock();
	for (int k = _BEG; k <= _END; k++) {
		cout << search_loop(_BEG, _END-1, k, A) << " " << search_recursive(_BEG, _END-1, k*3, A) << endl;
		// Ham search_loop se tim cac key k
		// Ham search_recursive tim cac key k*3
	}
	double exe = (double) (clock() - t) / CLOCKS_PER_SEC;
	cout << "Time exe: " << exe;
	return 0;
}

