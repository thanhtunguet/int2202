#include "iostream"
#include "cstdlib"
#include "ctime"
#include "windows.h"

using namespace std;

const int SZ = 10; // Kich thuoc toi da cua hang, cot

int mines[SZ][SZ];
bool opened[SZ][SZ];
int m, n, k;

int krand(int k) {
	srand((double) (rand() + time(NULL)) / k);
	return rand() % k;
}

bool is_cell(int x, int y) {
	return (x >= 0 && y >= 0 && x < m && y < n);
}

void count_mines() { // Dem so min o cac o xung quanh o an toan
	for (int x = 0; x < m; x++) {
		for (int y = 0; y < n; y++) {
			int newx, newy;
			if (mines[x][y] != -1) {
				for (int a = -1; a < 2; a++) {
					for (int b = -1; b < 2; b++) {
						newx = x+a;
						newy = y+b;
						if (is_cell(newx, newy)) {				// Neu o hop le
							if (newx != x || newy != y)			// Khong phai o trung tam
								if (mines[newx][newy] == -1) {	// Neu co min
									mines[x][y]++;
								}
						}
					}
				}
			}
		}
	}
}

void createMatrix() { // Tao ma tran min
	int mine = 0;
	int x, y;
	do {
		x = krand(m);
		y = krand(n);
		if (mines[x][y] != -1) {
			mines[x][y] = -1;
			mine++;
		}
	} while (mine < k);
	count_mines();
}

void printMatrix() { // In ra ma tran min
	system("cls");
	for (int y = 0; y <= n; y++) {
		if (y == 0) {
			cout << "~";
		} else {
			cout << "   ";
			cout << y;
		}
	}
	for (int x = 0; x < m; x++) {
		cout << endl << endl << x+1;
		for (int y = 0; y < n; y++) {
			cout << "   ";
			if (opened[x][y]) {
				if (mines[x][y] == -1) {
					cout << "*";
				} else {
					cout << mines[x][y];
				}
			} else {
				cout << "+";
			}
		}
	}
}

int loang(int x, int y) {
	int newx, newy;
	for (int a = -1; a < 2; a++) {
		for (int b = -1; b < 2; b++) {
			newx = x+a;
			newy = y+b;
			if (is_cell(newx, newy)) {
				// Neu dia chi hop le
				if (! opened[newx][newy]) {
					// Neu o chua duoc mo
					if (newx != x || newy != y) {
						// Khong phai o trung tam
						if (mines[newx][newy] == 0) {
							opened[newx][newy] = TRUE;
							loang(newx, newy);
						} else {
							if (mines[newx][newy] > 0) {
								opened[newx][newy] = TRUE;
							}
						}
					}
				}
			}
		}
	}
}

bool opencell(int x, int y, bool *dead) {
	opened[x][y] = TRUE; // Mo o
	if (mines[x][y] == -1) { // Neu co min
		*dead = TRUE;
		for (int x = 0; x < m; x++) {
			for (int y = 0; y < n; y++) {
				opened[x][y] = TRUE;
			}
		}
		return FALSE;
	} else {
		if (mines[x][y] == 0) {
			loang(x, y);
		}
		return TRUE;
	}
}

int main() {
	system("color A");
	cout << "Input m n k: (m, n <= 9, k <= m x n)" << endl;;
	cin >> m >> n >> k;
	for (int x = 0; x < m; x++) {
		for (int y = 0; y < n; y++) {
			mines[x][y] = 0;
			opened[x][y] = FALSE;
		}
	}
	createMatrix();
	bool dead = FALSE;
	int x, y;
	do {
		printMatrix();
		cout << "\n\n(x, y) = ";
		cin >> x >> y;
		x--; y--;
		if (! opencell(x, y, &dead)) {
			while (true) {
				system("color C");
				printMatrix();
				cout << endl << endl << "YOU'RE DEAD";
				Sleep(40);
				system("Color F");
				printMatrix();
				cout << endl << endl << "YOU'RE DEAD";
				Sleep(40);
			}
		}
	} while (! dead);
}

