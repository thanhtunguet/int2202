#include "iostream"
using namespace std;

double H(int N) { 
   if (N == 1) return 1.0;
   return H(N) + 1.0/N; 
}

int main() {
	cout << H(8);
	return 0;
}

