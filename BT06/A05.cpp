#include "iostream"
using namespace std;

#define long long long

long F(int n) { 
   if (n == 0) return 0; 
   if (n == 1) return 1; 
   return F(n-1) + F(n-2); 
}

long f(int n) {
	if (n == 1 || n == 2) {
		return 1;
	} else {
		long a = 1, f = 1, c;
		for (int i = 3; i <= n; i++) {
			c = a;
			a = f;
			f = a + c;
		}
		return f;
	}
}

int main() {
	cout << F(40);
	return 0;
}
// 0.01375 0.01549
// 1.485   0.01313
