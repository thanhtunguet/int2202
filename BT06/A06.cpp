#include "iostream"
#include "ctime"
#include "cstdlib"

using namespace std;

const int N = 3072;

int krand() {
	srand(rand() + time(NULL) / rand());
	return rand();
}

long long f(int k) {
	int A[N];
	long long sum = 0;
	if (k == 0) {
		return 0;
	}
	for (int i = 0; i < k; i++) {
		A[i] = krand();
		sum += A[i];
	}
	return sum + f(k - 1);
}

int main() {
	cout << f(150);
	return 0;
}

