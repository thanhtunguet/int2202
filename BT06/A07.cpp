#include "cstring"
#include "iostream"
using namespace std;

const int N = 100;

bool delChar(int pos, char str[N]) {
	int len = strlen(str);
	if (pos < len && pos >= 0) {
		for (int i = pos; i < len; i++) {
			str[i] = str[i+1];
		}	
	} else {
		return false;
	}
}

// Bai nay duyet hoan vi voi yeu cau
// Xau ki tu nhap vao khong co hai ki
// tu nao giong nhau

void hoan_vi(char str[N], char pre[N]) {
	int len = strlen(str);
	if (len == 1) {
		cout << endl << pre << str;
	} else {
		char prefix[N];
		char strr[N];
		for (int i = 0; i < len; i++) {
			strcpy(prefix, pre);
			strcpy(strr, str);
			sprintf(prefix, "%s%c", prefix, str[i]);
			delChar(i, strr);
			hoan_vi(strr, prefix);
		}
	}
}

int main() {
	char str[N];
	char pre[N];
	pre[0] = '\0';
	cin >> str;
	hoan_vi(str, pre);
	return 0;
}

