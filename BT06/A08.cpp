#include "iostream"
using namespace std;

const int MAXN = 100;

void in_kq(int A[MAXN], int k, int N) {
	cout << endl << "[";
	for (int i = 0; i < k; i++) {
		if (i > 0)
			cout << ", ";
		cout << A[i];
	}
	cout << "]";
}
 
bool duyet_xong(int A[MAXN], int k, int N) {
	for (int i = 0; i < k; i++) {
		if (A[i] < N-k+1+i)
			return false;
	}
	return true;
}
 
void thay_doi(int A[MAXN], int k, int N, int vt) {
	if (vt >= 0) {
		if (A[vt] < N-k+1+vt) {
			A[vt]++;
			for (int i = vt+1; i < k; i++) {
				A[i] = A[i-1]+1;
			}
		} else {
			thay_doi(A, k, N, vt-1);
		}
	}
}

void liet_ke(int A[MAXN], int k, int N) {
	in_kq(A, k, N);
	if (!duyet_xong(A, k, N)) {
		thay_doi(A, k, N, k-1);
		liet_ke(A, k, N);
	}
}

int main() {
	int N;
	cin >> N;
	int A[MAXN];
	for (int i = 0; i <= N; i++) {
		for (int j = 0; j <= i; j++) {
			A[j] = j+1;
		}
		liet_ke(A, i, N);
	}
	return 0;
}

