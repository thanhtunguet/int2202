#include "iostream"
#include "cstring"
using namespace std;

const int MAXN = 27; // Can dung ki tu '\0'

// Tan dung code A07

bool delChar(int pos, char str[MAXN]) {
	int len = strlen(str);
	if (pos < len && pos >= 0) {
		for (int i = pos; i < len; i++) {
			str[i] = str[i+1];
		}	
	} else {
		return false;
	}
}

// Bai nay duyet hoan vi voi yeu cau
// Xau ki tu nhap vao khong co hai ki
// tu nao giong nhau

void hoan_vi(char str[MAXN], char pre[MAXN]) {
	int len = strlen(str);
	if (len == 1) {
		cout << endl << pre << str;
	} else {
		char prefix[MAXN];
		char strr[MAXN];
		for (int i = 0; i < len; i++) {
			strcpy(prefix, pre);
			strcpy(strr, str);
			sprintf(prefix, "%s%c", prefix, str[i]);
			delChar(i, strr);
			hoan_vi(strr, prefix);
		}
	}
}

// Tan dung cach lam bai C03

void in_kq(char A[MAXN], int k, int N) {
	A[k] = '\0';
	hoan_vi(A, "");
}

bool duyet_xong(char A[MAXN], int k, int N) {
	for (int i = 0; i < k; i++) {
		if (A[i] != N - k + i + 'a') {
			return false;
		}
	}
	return true;
}

void thay_doi(char A[MAXN], int k, int N, int vt) {
	if (vt >= 0) {
		if (A[vt] < N-k+vt+'a') {
			A[vt]++;
			for (int i = vt+1; i < k; i++) {
				A[i] = A[i-1]+1;
			}
		} else {
			thay_doi(A, k, N, vt-1);
		}
	}
}

void liet_ke(char A[MAXN], int k, int N) {
	in_kq(A, k, N);
	if (!duyet_xong(A, k, N)) {
		thay_doi(A, k, N, k-1);
		liet_ke(A, k, N);
	}
}

int main() {
	int N, k;
	cin >> N >> k;
	char A[MAXN];
	for (int i = 0; i < N; i++) {
		A[i] = i+'a';
	}
	liet_ke(A, k, N);
	return 0;
}

