#include "iostream"
using namespace std;
const int MAXN = 26; // Khong can dung ki tu '\0'

// Tan dung cach lam A08

void in_kq(char A[MAXN], int k, int N) {
	cout << endl;
	bool sepa = false;
	for (int i = 0; i < k; i++) {
		if (sepa) {
			cout << " ";
		} else {
			sepa = true;
		}
		cout << A[i];
	}
}

bool duyet_xong(char A[MAXN], int k, int N) {
	for (int i = 0; i < k; i++) {
		if (A[i] != N - k + i + 'a') {
			return false;
		}
	}
	return true;
}

void thay_doi(char A[MAXN], int k, int N, int vt) {
	if (vt >= 0) {
		if (A[vt] < N-k+vt+'a') {
			A[vt]++;
			for (int i = vt+1; i < k; i++) {
				A[i] = A[i-1]+1;
			}
		} else {
			thay_doi(A, k, N, vt-1);
		}
	}
}

void liet_ke(char A[MAXN], int k, int N) {
	in_kq(A, k, N);
	if (!duyet_xong(A, k, N)) {
		thay_doi(A, k, N, k-1);
		liet_ke(A, k, N);
	}
}

int main() {
	int N, k;
	cin >> N >> k;
	char A[MAXN];
	for (int i = 0; i < N; i++) {
		A[i] = i+'a';
	}
	liet_ke(A, k, N);
	return 0;
}

