#include "iostream"
#include "cmath"
using namespace std;
const int MAXN = 32;
int ma_tran(int A[MAXN][MAXN], int N, int x, int y, bool inverse = false) {
	if (N == 1) {
		A[x][y] = inverse ? 1 : 0;
		A[x][y+1] = inverse ? 1 : 0;
		A[x+1][y] = inverse ? 1 : 0;
		A[x+1][y+1] = inverse ? 0 : 1;
	} else {
		ma_tran(A, N-1, x, y, inverse);
		ma_tran(A, N-1, x, y+pow(2,N)/2, inverse);
		ma_tran(A, N-1, x+pow(2,N)/2, y, inverse);
		ma_tran(A, N-1, x+pow(2,N)/2, y+pow(2,N)/2, !inverse);
	}
}
 void in_kq(int A[MAXN][MAXN], int N) {
 	int sz = pow(2, N);
 	for (int x = 0; x < sz; x++) {
 		if (x > 0)
 			cout << endl;
 		for (int y = 0; y < sz; y++) {
 		if (y > 0) {
 			cout << " ";
		 }
		 cout << ((A[x][y] == 1) ? 'O' : '.');
	} 			
		}
 }

int main() {
	int N;
	cin >> N;
	int A[MAXN][MAXN];
	ma_tran(A, N, 0, 0);
	in_kq(A, N);
	return 0;
}

