Do trong tuần em có việc bận nên em xin phép được nộp bài muộn
Mong cô thông cảm và chấm lại bài giúp em
Em xin cảm ơn cô.

Bài A02:
	Chương trình bị lỗi
	Do thiếu trường hợp cơ bản nên chương trình chạy vô hạn và bị sập
Bài A03:
	Chương trình bị lỗi
	Do sai công thức đệ quy, không đệ quy về trường hợp cơ bản nên lỗi
Bài A04:
	Chương trình không có lỗi
	Thời gian chạy test lâu hơn (giây)
	H(5000): 0.0208
	H(5):    0.0892
	HÀm
Bài A05:
	Hàm F: đệ quy
	Hàm f: vòng lặp
	Hàm F chạy nhanh hơn hàm f với các số nhỏ: < 10
	nhưng chậm hơn hàm f với các số lớn
	Kết quả test (giây):
	F(10): 0.01375
	f(10): 0.01549
	F(40): 1.485
	f(40):  0.01313
Bài A06:
	Hàm đệ quy khai báo một mảng cục bộ với kích thước tĩnh N = 3072
	Trường hợp cơ bản: k = 0
	Trường hợp đệ quy: f(k) sẽ gọi đến f(k-1) với k > 0
	Chương trình bị sập với k khoảng lớn hơn 150 trở đi