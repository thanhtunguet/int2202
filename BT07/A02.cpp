#include "iostream"
using namespace std;
const int N = 10;

int F(int A[]) {
	cout << sizeof(A);
}

int f(int A[N]) {
	cout << sizeof(A);
}

int main() {
	int A[N];
	cout << sizeof(A);
	cout << endl;
	cout << F(A);
	cout << endl;
	cout << f(A);
	// Mang duoc truyen vao duoi dang con tro
	// O main(): mang co kich thuoc: 10x4 = 40
	// O f(A) va F(A) cho ket qua giong nhau: 8 4745728
	// 8 la kich thuoc cua bien con tro
	// 4745728 la phan bo nho ma con tro tro toi
	// Gia tri nay thay doi tuy theo he dieu hanh dang chay
	return 0;
}

