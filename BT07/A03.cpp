#include "iostream"
#include "cstdlib"
using namespace std;

int count_even(int *A, int N) {
	int c = 0;
	for (int i = 0; i < N; i++) {
		if (A[i] % 2 == 0) {
			c++;
		}
	}
	return c;
}

int main() {
	int N;
	cin >> N;
	int *A;
	A = (int*) malloc(sizeof(int) * N);
	for (int i = 0; i < N; i++) {
		cin >> A[i];
	}
	if (N <= 5) {
		// N <= 5 thi xet ca mang luon
		cout << count_even(A, N);
	} else {
		// Neu N > 5 thi moi xet 5 phan tu dau
		cout << count_even(A, 5);
		cout << " ";
		// va 5 phan tu cuoi
		cout << count_even(&(A[N-5]), 5);
	}
	return 0;
}

