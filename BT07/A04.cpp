#include "iostream"
using namespace std;

const int N = 100;

int search_by_pointer(int *low, int *high, int *key, int A[N]) {
	if (*low < 0 || *high >= N) {
		return -1;
	} else {
		if (A[*low] > *key || A[*high] < *key) {
			return -1;
		} else {
			if (A[*low] == *key) {
				return *low;
			} else if (A[*high] == *key) {
				return *high;
			} else {
				int mid = (*low+*high) / 2;
				if (A[mid] == *key) {
					return mid;
				} else if (A[mid] > *key) {
					mid--;
					search_by_pointer(low, &mid, key, A);
				} else {
					mid++;
					search_by_pointer(&mid, high, key, A);
				}
			}
		}
	}
}

int main() {
	int A[N];
	for (int i = 0; i < N; i++) {
		A[i] = (i+1) * 3;
	}
	int low = 4, high = 99, key = 14;
	cout << search_by_pointer(&low, &high, &key, A);
	return 0;
}

