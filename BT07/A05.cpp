#include "iostream"
#include "cstdlib"
using namespace std;

const int N = 10000;

char* weird_string() { 
   char c[] = "123456";
   return c; 
}

int F() {
	char A[N];
	for (int i = 0; i < N; i++) {
		A[i] = '0';
	}
	A[N-1] = '\0';
}

int main() {
	char *c;
	c = weird_string();
	/**
	 * Khi vua thuc hien weird_string(), stack da bi thu hoi
	 * nhung chua bi ghi de, nen gia tri ma c tro toi van
	 * la "123456"
	 */
	cout << c;
	F();
	/**
	 * Neu goi F(), stack cung cap cho F() se ghi de len
	 * stack vua cung cap cho weird_string()
	 * Con tro c dang tro toi vung nho thuoc stack do nen
	 * Gia tri ma c tro toi khong con la "123456"
	 */
	cout << endl;
	cout << c;
	/**
	 * Trinh bien dich canh bao: [Warning] address of local variable 'c' returned [-Wreturn-local-addr]
	 */
	return 0;
}

