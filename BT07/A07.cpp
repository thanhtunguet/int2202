#include "iostream"
#include "cstring"
#include "cstdlib"
using namespace std;

int main(int argc, char *argv[]) {
	if (argc >= 3) {
		int
			len1 = strlen(argv[1]),
			len2 = strlen(argv[2]);
		if (len1 > len2) {
			cout << 0;
		} else if (len1 == len2) {
			if (strcmp(argv[1], argv[2]) == 0) {
				cout << 1;
			} else {
				cout << 0;
			}
		} else {
			char *str;
			str = (char*) malloc(sizeof(char) * (len1+1));
			int c = 0;
			for (int i = len2 - len1; i >= 0; i--) {
				for (int j = 0; j < len1; j++) {
					str[j] = argv[2][i+j];
				}
				str[len1] = '\0';
				if (strcmp(argv[1], str) == 0) {
					c++;
				}
			}
			cout << c;
		}
	}
}
