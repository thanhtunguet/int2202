B1:
	<!-- Output -- >
*p3 = B, p3 = 0x5678
*p3 = A, p3 = 0x1234
*p1 = B, p1 = 0x1234
	<!-- Output -->

B2:
	*p = 5;
B3:
[Error] cannot convert 'char*' to 'double*' in initialization
	Không thể chuyển đổi kiểu con trỏ char sang con trỏ double
	Vì kích thước dữ liệu cho char và double khác nhau mà kích
	thước các biến con trỏ lại giống nhau, con trỏ chỉ có chức
	năng trỏ đến địa chỉ vùng nhớ đó chứ không chứa giá trị của vùng nhớ đó
	Khác với quá trình đổi kiểu dữ liệu từ char sang double, chương
	trình sẽ tính toán giá trị nhị phân được lưu trong vùng nhớ kiểu char
	để đổi sang giá trị double tương ứng để lưu vào vùng nhớ mới
B5:
	Khai báo int *p = 10 là không hợp lệ
B6:
	*p = n;
B7:
	p1 = p2;
B8:
	Phép gán
B9:
	Tất cả các phát biểu
B10:
	*(p+2)
B11:
	1
B12:
	*p+1 là giá trị của p1
B13:
	Phải cung cấp độ dài mảng động ngay khi khai báo
B14:
	Là kiểu truyền giá trị
B15:
