#include "iostream"
using namespace std;

void f(int xval)
{
   int x;
   x = xval;
   cout << &x;
   // in d?a ch? c?a x t?i d�y
}
void g(int yval)
{
   int y;
   // in d?a ch? c?a y t?i d�y
   cout << &y;
}
int main()
{
   f(7);
   cout << " ";
   g(11);
   // Do ham g() da duoc cap stack cua chinh ham f() vua thuc hien xong
   // Hai ham co cung kich thuoc du lieu nen hai tham so dau tien va cung la duy nhat duoc xep cung dia chi
   return 0;
}
