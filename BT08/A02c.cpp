#include "iostream"
using namespace std;

int main( )
{ 
   double a[4] = {1, 2, 3, 4}; 
   for (double *cp = a; *cp != a[4]; cp++) {
   	cout << cp << ": " << *cp << endl;
   }
   return 0;
}
