#include "iostream"
using namespace std;

// Ket qua bi sai
// Do thuc hien nhay buoc nen a[1] va a[3] bi bo qua
// Ket qua phu thuoc vao a[4] tro di

int main( )
{ 
   double a[4] = {1, 2, 3, 4}; 
   for (double *cp = a; *cp != a[4]; cp+=2) {
   	cout << cp << ": " << *cp << endl;
   }
   return 0;
}
