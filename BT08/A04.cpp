#include "iostream"
#include "cstdlib"
using namespace std;

int main()  {
    char **s;
    /**
	 * Loi thu nhat: su dung bien mang nhu con tro
	 * Bien mang duoc truyen vao ham theo co che giong con tro nhung khong phai la con tro
	 * Giai quyet: muon su dung nhu con tro thi phai khai bao duoi dang con tro
     * //char foo[] = "Hello World";
	 */
	char *foo = "Hello World";
    s = &foo;
    /**
	 * loi thu hai: Ham printf nhan dinh dang %s la char*, khong phai char**
	 * Giai quyet: doi s thanh *s
	 * // printf("s is %s\n", s);
	 */
    printf("s is %s\n", *s);
    /**
	 * Loi thu ba: Con tro s chua khai bao kich thuoc!
	 * Nen khong dung s[0] duoc
	 * mac du theo cu phap s[0] la mot con tro
	 */
  //  s[0] = foo;
  //  printf("s[0] is %sn",s[0]);
    return 0;
}
