#include "iostream"
#include "cstdlib"
#include "cstdio"
using namespace std;

int str_length(const char str[]) {
	int i;
	for (i = 0; str[i] != '\0'; i++);
	return i;
}

int reverse(char *str) {
	int len = str_length(str);
	char *newstr = (char*) malloc(len+1);
	for (int i = 0; i <= len; i++) {
		newstr[i] = str[i];
	}
	for (int i = len-1; i >= 0; i--) {
		str[i] = newstr[len-i-1];
	}
}

int delete_char(char *str, const char c) {
	int len = str_length(str);
	for (int i = len-1; i >= 0; i--) {
		if (str[i] == c) {
			len--;
			for (int j = i; j <= len; j++) {
				str[j] = str[j+1];
			}
		}
	}
}

int pad_right(char *str, const int n) {
	int len = str_length(str);
	for (int i = len; i < n; i++) {
		str[i] = ' ';
	}
	str[n] = '\0';
}

int pad_left(char *str, const int n) {
	int len = str_length(str);
	int pad = n - len;
	for (int i = 0; i < pad; i++) {
		len++;
		for (int i = len; i >= 1; i--) {
			str[i] = str[i-1];
		}
		str[0] = ' ';
	}
}

int truncate(char *str, const int n) {
	int len = str_length(str);
	if (len > n) {
		str[n] = '\0';
	}
}

bool is_palindrome(const char *str) {
	int
		lo = 0,
		hi = str_length(str) - 1;
	while (lo < hi) {
		if (str[lo] != str[hi]) {
			return false;
		}
		lo++;
		hi--;
	}
	return true;
}

int trim_left(char *str) {
	int len = str_length(str);
	int spaces = 0;
	for (spaces = 0; str[spaces] == ' '; spaces++);
	for (int i = spaces; i <= len; i++) {
		str[i - spaces] = str[i];
	}
}

int trim_right(char *str) {
	int len = str_length(str);
	int n = len - 1;
	while (str[n] == ' ') {
		len--;
		n--;
		str[len] = '\0';
	}
}

int get_string(char *str) {
	char c;
	int i = 0;
	fflush(stdin);
	while (c = fgetc(stdin)) {
		if (c == '\n') {
			break;
		} else {
			str[i] = c;
			i++;
		}
	}
}

int main() {
	char str[255];
	
	char *cmd[] = {
		"reverse", "delete_char", "pad_right", "pad_left", "truncate", "is_palindrome", "trim_left", "trim_right"
	};
	for (int i = 0; i < 8; i++) {
		cout << i << ": " << cmd[i] << endl;
	}
	
	// Bat dau demo
	cout << "Demo: cin n << ";
	int n;
	char c;
	cin >> n;
	cout << "Function " << cmd[n] << endl;
	cout << "Type your string, press Enter to finish: str << ";
	get_string(str);
	switch (n) {
		case 0:
			reverse(str);
			cout << "\t>> Result = " << str;
			break;
		case 1:
			cout << "c = ";
			cin >> c;
			delete_char(str, c);
			cout << "\t>> Result = " << str;
			break;
		case 2:
			cout << "n = ";
			cin >> n;
			pad_right(str, n);			
			cout << "\t>> Result = " << str;
			break;
		case 3:
			cout << "n = ";
			cin >> n;
			pad_left(str, n);			
			cout << "\t>> Result = " << str;
			break;
		case 4:
			cout << "n = ";
			cin >> n;
			truncate(str, n);			
			cout << "\t>> Result = " << str;
			break;
		case 5:
			cout << (is_palindrome(str) ? "YES" : "NO");
			break;
		case 6:
			trim_left(str);
			cout << "\t>> Result = " << str;
			break;
		case 7:
			trim_right(str);
			cout << "\t>> Result = " << str;
			break;
	}
	return 0;
}

