#include "iostream"
#include "cstring"
using namespace std;

char *concat(const char *A, const char *B) {
	int
		lenA = strlen(A),
		lenB = strlen(B);
	char *dst = new char[lenA+lenB+1];
	for (int i = 0; i < lenA; i++) {
		dst[i] = A[i];
	}
	for (int i = 0; i <= lenB; i++) {
		dst[i+lenA] = B[i];
	}
	return dst;
}

int main() {
	char *A = "Hello";
	char *B = " world";
	cout << concat(A, B);
	return 0;
}

