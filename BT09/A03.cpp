#include "iostream"
using namespace std;

int main() {
	char* a = new char[10];
	char* c = a + 3;
	for (int i = 0; i < 9; i++) a[i] = 'a'; 
	a[9] = '\0';
	cerr <<"a: " << "-" << a << "-" << endl;
	cerr <<"c: " << "-" << c << "-" << endl;
	delete c; // Loi o day la do giai phong het vung bo nho tu c tro di, lam cho a chi con 3 ki tu nhung khong co ki tu ket thuc xau '\0'
	// chuong trinh crash tren windows (Dev C++)
	cerr << "a after deleting c:" << "-" << a << "-" << endl;
	return 0;
}

