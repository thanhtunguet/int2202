#include "iostream"
using namespace std;

int str_length(const char *str) {
	int len;
	for (len = 0; str[len] != '\0'; len++);
	return len;
}

char *createString(const int len) {
	char *dst = new char[len];
	return dst;
}

char *copyString(const char *src) {
	int len = str_length(src);
	char *dst = createString(len+1);
	for (int i = 0; i <= len; i++) {
		dst[i] = src[i];
	}
	return dst;
}

char *reverse(const char *src) {
	int len = str_length(src);
	char *dst = createString(len+1);
	for (int i = 0; i < len; i++) {
		dst[i] = src[len-i-1];
	}
	dst[len] = '\0';
	return dst;
}

char *delete_char(const char *src, const char ch) {
	int len = str_length(src);
	char *dst = createString(len+1);
	char *cp = dst;
	for (int i = 0; i <= len; i++) {
		if (src[i] != ch) {
			*cp = src[i];
			cp++;
		}
	}
	return dst;
}

char *pad_right(const char *src, const int N) {
	int len = str_length(src);
	if (len >= N) {
		return copyString(src);
	} else {
		char *dst = createString(N+1);
		for (int i = 0; i < len; i++) {
			dst[i] = src[i];
		}
		for (int i = len; i < N; i++) {
			dst[i] = ' ';
		}
		dst[N] = '\0';
		return dst;
	}
}

char *pad_left(const char *src, const int N) {
	int len = str_length(src);
	if (len >= N) {
		return copyString(src);
	} else {
		char *dst = createString(N+1);
		int pad = N - len;
		for (int i = 0; i < pad; i++) {
			dst[i] = ' ';
		}
		for (int i = pad; i <= N; i++) {
			dst[i] = src[i-pad];
		}
		return dst;
	}
}

char *truncate(const char *src, const int N) {
	int len = str_length(src);
	char *dst = copyString(src);
	if (len > N) {
		dst[N] = '\0';
	}
	return dst;
}

char *trim_left(const char *src) {
	int i;
	int len = str_length(src);
	for (i = 0; i < len; i++) {
		if (src[i] != ' ')
			break;
	}
	return copyString(src+i);
}

char *trim_right(const char *src) {
	int i = 0;
	int len = str_length(src);
	for (int j = len - 1; j >= 0; j--) {
		if (src[j] == ' ') {
			i++;
		} else {
			break;
		}
	}
	char *dst = createString(len-i+1);
	i = len - i;
	for (int j = 0; j < i; j++) {
		dst[j] = src[j];
	}
	dst[i] = '\0';
	return dst;
}

int main() {
	char *str = new char[100];
	gets(str);
	cout << "<" << trim_right(str) << ">";
	return 0;
}

