#include "iostream"
using namespace std;

struct Point {
	double x;
	double y;
};

void printPoint(Point p) {
	cout << "(" << p.x << ", " << p.y << ")";
}

int main() {
	Point p;
	cin >> p.x >> p.y;
	printPoint(p);
	return 0;
}

