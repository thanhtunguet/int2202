#include "iostream"
using namespace std;

struct Point {
	double x;
	double y;
};

// Hai ham cung ten, co the tuy bien kieu truyen du lieu

void printPointAddr(Point p) {
	cout << &p;
}

void printPointAddr(Point *p) {
	cout << p;
}

int main() {
	Point p;
	cout << &p << endl;
	printPointAddr(p);
	cout << " ";
	printPointAddr(&p);
	return 0;
}

