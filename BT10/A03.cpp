#include "iostream"
using namespace std;

struct Point {
	double x;
	double y;
};

Point *mid_point(const Point A, const Point B) {
	Point *p = new Point;
	p->x = (A.x+B.x) / 2;
	p->y = (A.y+B.y) / 2;
	return p;
}

int main() {
	Point *p, A, B;
	A = {3, 5};
	B = {4, 10};
	p = mid_point(A, B);
	cout << p->x << " " << p->y;
	return 0;
}

