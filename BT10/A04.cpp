#include "iostream"
using namespace std;

struct Point {
	double x;
	double y;
};

void printPointAttr(Point *p) {
	cout << p << " " << &p->x << " " << &p->y;
}

int main() {
	Point p;
	printPointAttr(&p);
	// x la truong dau tien, co cung dia chi voi p
	// y la truong thu hai, co dia chi lien ke voi x
	return 0;
}

