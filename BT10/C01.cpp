#include "iostream"
#include "cstring"
using namespace std;

// Em xin phep duoc thay doi ten ca truong
// str thanh value
// n thanh length

struct String {
	int length;
	char *value;
	
	String(const char *str = NULL) {
		if (str == NULL) {
			this->length	= 0;
			this->value		= NULL;
		} else {
			this->length	= strlen(str);
			this->value		= new char[this->length];
			for (int i = 0; i < this->length; i++) {
				this->value[i] = str[i];
			}
		}
	}
	
	void print() {
		if (this->length > 0) {
			for (int i = 0; i < this->length; i++) {
				cout << this->value[i];
			}
		}
	}
	
	void append(const char *str) {
		int len = strlen(str);
		if (len > 0) {
			char *temp = new char[len+this->length];
			for (int i = 0; i < this->length; i++) {
				temp[i] = this->value[i];
			}
			for (int i = 0; i < len; i++) {
				temp[i+this->length] = str[i];
			}
			delete [] this->value;
			this->value = temp;
			this->length += len;
		}
	}
	
	// Tra ve cstring cho cac ham khac su dung
	// Ham nay do em viet them
	
	char *return_cstring() {
		if (this->length > 0) {
			char *str = new char[this->length+1];
			for (int i = 0; i < this->length; i++) {
				str[i] = this->value[i];
			}
			str[this->length] = '\0';
			return str;
		} else {
			return "";
		}
	}
	
	~String() {
		if (this->length > 0) {
			delete [] this->value;
		}
	}
};

int main() {
	String *s = new String("Hello world");
	s->print();
	cout << endl;
	s->append(" la mot bai tap hay nhat");
	s->print();
	char *str = s->return_cstring();
	cout << endl << str;
	return 0;
}

