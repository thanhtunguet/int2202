#include "iostream"
#include "cstring"
using namespace std;

struct Node {
	//
	// Instance Variables
	//
	const char *element;
	Node *next;
	// Contructor
	Node(const char *str = NULL, Node *N = NULL) {
		element	= str;
		next	= N;
	}
};

struct SLinkedList {
	Node *head;	// Pointer point to first node
	Node *tail;
	long size;	// Nums of nodes

	SLinkedList() {
		head = NULL;
		size = 0;
		tail = NULL;
	}

	~SLinkedList() {
		size = 0;
		Node *temp = head;
		while (head != NULL) {
			delete head;
			head = temp->next;
			temp = head;
		}
		tail = NULL;
	}

	void addFirst(const char *str = NULL) {
		Node *newNode = new Node(str, head);
		head = newNode;
		size++;
		if (size == 1) {
			tail = head;
		}
	}

	void addLast(const char *str = NULL) {
		Node *newNode = new Node(str);
		size++;
		if (size == 1) {
			head = newNode;
		} else {
			tail->next = newNode;
		}
		tail = newNode;
	}

	void insertAfter(Node *p, const char *str = NULL) {
		Node *newNode = new Node(str, p->next);
		p->next = newNode;
	}

	void print() {
        Node *temp = head;
        while (temp != tail) {
            cout << temp->element << endl;
            temp = temp->next;
        }
        cout << temp->element;
	}

	void removeFirst() {
		size--;
        if (size > 0) {
            Node *temp = head;
            delete head;
            head = temp->next;
        } else {
			delete head;
			head = NULL;
			tail = NULL;
        }
	}

	void removeLast() {
		size--;
		if (size == 0) {
			delete tail;
			head = NULL;
			tail = NULL;
		} else {
			Node *newNode = head;
			while (newNode->next != tail) {
				newNode = newNode->next;
			}
			delete tail;
			tail = newNode;
		}
	}
	
	char *removeNode(Node *p) {
		int plen = strlen(p->element);
		char *str = new char[plen+1];
		for (int i = 0; i <= plen; i++) {
			str[i] = p->element[i];
		}
		size--;
		if (p == head) {
			head = p->next;
		} else {
			Node *prev = NULL;
			Node *srch = head;
			while (srch != p) {
				prev = srch;
				srch = srch->next;
			}
			if (p == tail) {
				tail = prev;
			}
			prev->next = p->next;
			//cout <<  endl << prev  << " " << prev->next << " " << srch << " " << srch->next << endl;
		}
		delete p;
		return str;
	}
};

void testA1() {
	SLinkedList *SL = new SLinkedList();
	cout << "Initialize variable: new SLinkedList in testA1()" << endl;
    SL->addFirst("First string data");
    cout << "Call to method: addFirst():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->addLast("Last string data");
    cout << "Call to method: addLast():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	cout << "End testA1();" << endl << endl;
    delete SL;
}

void testA2() {
	SLinkedList *SL = new SLinkedList();
	cout << "Initialize variable: new SLinkedList in testA2()" << endl;
    SL->addFirst("First string data");
    cout << "Call to method: addFirst():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->addLast("Last string data");
    cout << "Call to method: addLast():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->removeFirst();
	cout << "Call to method: removeFirst():" << endl;
	cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	cout << "End testA2();" << endl << endl;
    delete SL;
}

void testA3() {
	SLinkedList *SL = new SLinkedList();
	cout << "Initialize variable: new SLinkedList in testA3()" << endl;
    SL->addFirst("First string data");
    cout << "Call to method: addFirst():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->addLast("Last string data");
    cout << "Call to method: addLast():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	delete SL;
	cout << "End testA3();" << endl << endl;
}

void testA4() {
	SLinkedList *SL = new SLinkedList();
	cout << "Initialize variable: new SLinkedList in testA4()" << endl;
    SL->addFirst("First string data");
    cout << "Call to method: addFirst():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->addLast("Last string data");
    cout << "Call to method: addLast():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->removeLast();
	cout << "Call to method: removeLast():" << endl;
	cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	cout << "End testA4();" << endl << endl;
    delete SL;
}

void testA5() {
	SLinkedList *SL = new SLinkedList();
	cout << "Initialize variable: new SLinkedList in testA5()" << endl;
    SL->addFirst("First string data");
    cout << "Call to method: addFirst():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->addLast("Last string data");
    cout << "Call to method: addLast():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->insertAfter(SL->head, "Insert after");
	cout << "Call to method: insertAfter():" << endl;
	cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	cout << "End testA5();" << endl << endl;
    delete SL;
}

void testA6() {
	SLinkedList *SL = new SLinkedList();
	cout << "Initialize variable: new SLinkedList in testA6()" << endl;
    SL->addFirst("First string data");
    cout << "Call to method: addFirst():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->addLast("Last string data");
    cout << "Call to method: addLast():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	SL->removeNode(SL->head->next);
	cout << "Call to method: removeNode():" << endl;
    cout << "<!-- Print() -->" << endl;
    SL->print();
	cout << endl << "----------------" << endl;
	delete SL;
	cout << "End testA6();" << endl << endl;
}

int main() {
	testA1();
	testA2();
	testA3();
	testA4();
	testA5();
	testA6();
}
