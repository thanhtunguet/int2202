using namespace std;

#include "Fraction.cpp"

int main() {
	Fraction F(5, 15), G(5, 6);
	Fraction H = F + G;
	cout << "H + G = ";
	H.print();
	cout << endl;
	H = F - G;
	cout << "H - G = ";
	H.print();
	cout << endl;
	H = F * G;
	cout << "H * G = ";
	H.print();
	cout << endl;
	H = F / G;
	cout << "H / G = ";
	H.print();
};
