#include "iostream"
using namespace std;


class DNode {
	public:
	const char *element;
	DNode *next;
	DNode *prev;
	
	DNode(const char *str = NULL, DNode *P = NULL, DNode *N = NULL) {
		element = str;
		next = N;
		prev = P;
	}
};

class DList {
	DNode header;
	DNode trailer;

public:
	
	friend void test1();
	friend void test2();
	friend void test3();
	friend void test4();
	
	DList() {
		header.next = &trailer;
		trailer.prev = &header;
	}
	
	void addFirst(const char *str) {
		DNode *Dn = new DNode(str, &header, header.next);
		header.next->prev = Dn;
		header.next = Dn;
	}
	
	void addLast(const char *str) {
		DNode *Dn = new DNode(str, trailer.prev, &trailer);
		trailer.prev->next = Dn;
		trailer.prev = Dn;
	}
	
	void print() {
		cout << endl << "Begin Print() <<<" << endl;
		DNode *tmp = header.next;
		while (tmp != &trailer) {
			cout << tmp->element << endl;
			tmp = tmp->next;
		}
		cout << ">>> End Print()" << endl << endl;
	}
	
	void removeFirst() {
		if (header.next != &trailer) {
			DNode *Dn = header.next;
			header.next = header.next->next;
			header.next->prev = &header;
			delete Dn;
		}
	}
	
	void removeLast() {
		if (header.next != &trailer) {
			DNode *Dn = trailer.prev->prev;
			delete Dn->next;
			Dn->next = &trailer;
			trailer.prev = Dn;
		}
	}
	
	~DList() {
		DNode *tmp = header.next;
		while (tmp->next != &trailer) {
			tmp = tmp->next;
			delete tmp->prev;
		}
	}
};

void test1() {
	DList dll;
	cout << "--- Start test1()" << endl;
	dll.addFirst("First string data");
	dll.print();
	dll.addLast("Last string data");
	dll.print();
	dll.removeFirst();
	dll.print();
	cout << "--- Finish test1()" << endl << endl;
}


void test2() {
	DList dll;
	cout << "--- Start test2()" << endl;
	dll.addFirst("First string data");
	dll.addLast("Last string data");
	dll.print();
	dll.removeFirst();
	dll.print();
	cout << "--- Finish test2()" << endl << endl;
}

void test3() {
	DList *dll = new DList();
	cout << "--- Start test3()" << endl;
	dll->addFirst("First string data");
	dll->addLast("Last string data");
	dll->print();
	delete dll;
	cout << "--- Finish test3()" << endl << endl;
}

void test4() {
	DList dll;
	cout << "--- Start test4()" << endl;
	dll.addFirst("First string data");
	dll.addLast("Last string data");
	dll.print();
	dll.removeLast();
	dll.print();
	cout << "--- Finish test4()" << endl << endl;
}

int main(int argc, char *argv[]) {
	test1();
	test2();
	test3();
	test4();
	return 0;
}

