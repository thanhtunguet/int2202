#ifndef FRACTION_H
#define FRACTION_H
class Fraction {

public:
	
	int Nume, Deno;
	
	Fraction(const int N, const int D);
	
	void print();
	
	void simplify();
	
	Fraction operator+(const Fraction other) const;
	
	Fraction operator-(const Fraction other) const;
	
	Fraction operator*(const Fraction other) const;
	
	Fraction operator/(const Fraction other) const;

};
#endif
#ifndef _INCLUDED_LIB_
#define _INCLUDED_LIB_
	#include "iostream"
	#include "cmath"
	#include "cassert"
#endif
