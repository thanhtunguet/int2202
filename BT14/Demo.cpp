#include "iostream"
#include "cmath"
#include "cassert"
#include "algorithm"
#include "Fraction.cpp"

using namespace std;

void testA1() {
	Fraction f1(1,2), f2(1,1);
	cout << "Init f1 = 1/2; f2 = 1 in testA1()" << endl;
	Fraction sum = f1 + f2 + f1;
	sum.print();
	cout << endl << "End testA1()" << endl;
}

void testA2() {
	Fraction f1(3, 5), f2(5, 9);
	cout << "Init f1 = 3/5; f2 = 5/9 in testA2()" << endl;
	Fraction _max = get_max(f1, f2);
	_max.print();
	cout << endl << "End testA2();" << endl;
}

void testA3() {
	Fraction f1(3, 5), f2(5, 9);
	cout << "Init f1 = 3/5; f2 = 5/9 in testA3()" << endl;
	if (f1 > f2) {
		cout << "f1 > f2" << endl;
	} else {
		cout << "f1 < f2" << endl;
	}
	
	if (f1 < f2) {
		cout << "f1 < f2" << endl;
	} else {
		cout << "f2 < f1" << endl;
	}
}

void testA4() {
	Fraction F1(3, 5), F2(466, 595);
	cout << "Init F1 = 3/5; F2 = 466/595 in testA4()" << endl;
	Fraction Max_F = get_max(F1, F2);
	Max_F.print();
	cout << endl << "End testA4()" << endl;
}

void testA5() {
	Fraction F1(3, 5), F2(466, 695);
	cout << "Init F1 = 3/5; F2 = 466/695 in testA5()" << endl;
	Fraction Max_F = max(F1, F2);
	Max_F.print();
	cout << endl << "End testA5()" << endl;
}

void testA6() {
	Fraction F1(3, 5), F2(25, 5);
	cout << "Init F1 = 3/5; F2 = 25/5 = 5 in testA6()" << endl;
	cout << "Using << operator:\n\n\t'cout << F1 << \" \" << F2 << endl;'" << endl;
	cout << F1 << " " << F2 << endl;
	cout << "End testA6();" << endl;
}

int main() {
	testA1();
	cout << endl;
	testA2();
	cout << endl;
	testA3();
	cout << endl;
	testA4();
	cout << endl;
	testA5();
	cout << endl;
	testA6();
	
	// TEST CIN >> OPERATOR
	
	Fraction F(4, 5);
	cin >> F;
	cout << endl << "F = " << F << endl;
	return 0;
};
