#include "Fraction.h"

/**
 * We consider a fraction with denominator
 * equals to 0 has a value of 0 without error
 */

// Greatest common divisor

int gcd(int a, int b) {
	a = abs(a);
	b = abs(b);
	if (a == 0 || b == 0)
		return 1;
	if (b % a == 0)
		return a;
	if (a % b == 0)
		return b;
	if (a > b) {
		int t = a;
			a = b;
			b = t;
	}
	return gcd(b % a, a);
}

// Define Fraction's members

Fraction::Fraction(int N, int D) {
	assert(D != 0);	
	Nume = N;
	Deno = D;
	this->simplify();
}

void Fraction::simplify() {
	if (Nume == 0 || Deno == 0) {
		Deno = 1;
	} else {
		if (Deno < 0) {
			Nume = - Nume;
			Deno = - Deno;
		}
	}
	int G = gcd(Nume, Deno);
	Nume /= G;
	Deno /= G;
}

void Fraction::print() {
	if (Nume == 0 || Deno == 0) {
		std::cout << 0;
	} else {
		if (Nume % Deno == 0) {
			std::cout << Nume / Deno;
		} else {
			std::cout << Nume << '/' << Deno;
		}
	}
}

// Operators overloading

	Fraction Fraction::operator+(const Fraction other) const {
		assert(other.Deno != 0);
		int N = Nume * other.Deno + Deno * other.Nume;
		int D = Deno * other.Deno;
		Fraction sum(N, D);
		sum.simplify();
		return sum;
	}
	 
	Fraction Fraction::operator-(const Fraction other) const {
		assert(other.Deno != 0);
		int N = Nume * other.Deno - Deno * other.Nume;
		int D = Deno * other.Deno;
		Fraction diff(N, D);
		diff.simplify();
		return diff;
	}

	Fraction Fraction::operator*(const Fraction other) const {
		assert(other.Deno != 0);
		int N = Nume * other.Nume;
		int D = Deno * other.Deno;
		Fraction prd(N, D);
		prd.simplify();
		return prd;
	}

	Fraction Fraction::operator/(const Fraction other) const {
		assert(other.Deno != 0);
		int N = Nume * other.Deno;
		int D = Deno * other.Nume;
		Fraction qtnt(N, D);
		qtnt.simplify();
		return qtnt;
	}

	bool Fraction::operator>(const Fraction other) const {
		return (this->Nume * other.Deno - this->Deno * other.Nume > 0);
	}

	bool Fraction::operator<(const Fraction other) const {
		return (this->Nume * other.Deno - this->Deno * other.Nume < 0);
	}

// Maxium template

template <typename T>
inline T get_max(const T A, const T B) {
	return (A > B) ? A : B;
}

// outStream overloading

std::ostream& operator<<(std::ostream &out, Fraction &F) {
	if (F.Nume == 0)
		out << 0;
	else if (F.Nume % F.Deno == 0)
		out << F.Nume / F.Deno;
	else
		out << F.Nume << "/" << F.Deno;
	return out;
}

// inpStream overloading

std::istream& operator>>(std::istream &inp, Fraction &F) {
	inp >> F.Nume >> F.Deno;
	return inp;
}

// End Defining
