#ifndef FRACTION_H
#define FRACTION_H

class Fraction {
	
	int Nume, Deno;
	
	// Set Nume, Deno private;
	
public:

	Fraction(const int N, const int D);
	
	void print();
	
	void simplify();
		
	Fraction operator+(const Fraction other) const;
	
	Fraction operator-(const Fraction other) const;
	
	Fraction operator*(const Fraction other) const;
	
	Fraction operator/(const Fraction other) const;
	
	bool operator>(const Fraction other) const;

	bool operator<(const Fraction other) const;
	
	friend std::ostream& operator<<(std::ostream &out, Fraction &F);
	
	friend std::istream& operator>>(std::istream &inp, Fraction &F);
};
#endif
