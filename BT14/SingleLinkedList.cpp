// Code SingleLinkedList (BT12: A1.cpp)

#include "iostream"
#include "cstring"
using namespace std;

struct Node {
	
	const char *element;
	Node *next;
	
	Node(const char *str = NULL, Node *N = NULL) {
		element	= str;
		next	= N;
	}
};

struct SLinkedList {
	Node *head;	// Pointer point to first node
	Node *tail;
	long size;	// Nums of nodes

	SLinkedList() {
		head = NULL;
		size = 0;
		tail = NULL;
	}
// Redeclare destructor
	~SLinkedList() {
		size = 0;
		Node *temp;
		while (head != NULL) {
			temp = head;
			head = head->next;
			delete temp;
		}
		cout << endl << "Struct has been destructed;";
	}

	void addFirst(const char *str = NULL) {
		Node *newNode = new Node(str, head);
		head = newNode;
		size++;
		if (size == 1) {
			tail = head;
		}
	}

	void addLast(const char *str = NULL) {
		Node *newNode = new Node(str);
		size++;
		if (size == 1) {
			head = newNode;
		} else {
			tail->next = newNode;
		}
		tail = newNode;
	}

	void insertAfter(Node *p, const char *str = NULL) {
		Node *newNode = new Node(str, p->next);
		p->next = newNode;
	}

	void print() {
        Node *temp = head;
        while (temp != tail) {
            cout << temp->element << endl;
            temp = temp->next;
        }
        cout << temp->element;
	}

	void removeFirst() {
		size--;
        if (size > 0) {
            Node *temp = head;
            delete head;
            head = temp->next;
        } else {
			delete head;
			head = NULL;
			tail = NULL;
        }
	}

	void removeLast() {
		size--;
		if (size == 0) {
			delete tail;
			head = NULL;
			tail = NULL;
		} else {
			Node *newNode = head;
			while (newNode->next != tail) {
				newNode = newNode->next;
			}
			delete tail;
			tail = newNode;
		}
	}
	
	char *removeNode(Node *p) {
		int plen = strlen(p->element);
		char *str = new char[plen+1];
		for (int i = 0; i <= plen; i++) {
			str[i] = p->element[i];
		}
		size--;
		if (p == head) {
			head = p->next;
		} else {
			Node *prev = NULL;
			Node *srch = head;
			while (srch != p) {
				prev = srch;
				srch = srch->next;
			}
			if (p == tail) {
				tail = prev;
			}
			prev->next = p->next;
		}
		delete p;
		return str;
	}
};

SLinkedList testA() {
	SLinkedList SL;
	SL.addFirst("String 1");
	SL.addLast("String 2");
	SL.addLast("String 3");
	return SL;
}

int main() {
	
	SLinkedList SL = testA();
	SL.print();
}
